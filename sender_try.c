#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <inttypes.h>
#include <sys/time.h>

#define SEND_FILE 0
#define STORE 1
#define ACK_OK 2
#define ACK_FAIL 3
#define SENDING_DONE 4
#define WRONG_FILE 5 // error, no file with given name, stop sending
#define EMPTY 6

#define IP_PROTOCOL 0
#define PORT_NO_RECIEVER 14000
#define PORT_NO_SENDER 15000
#define sendrecvflag 0
#define nofile "File Not Found!"
#define IP_ADDRESS "127.0.0.1"//"192.168.100.251"
#define DATA_LENGTH_CRC 1012
#define DATA_LENGTH_HASH 1016
#define NET_BUF_SIZE 1020 // length of sended data

static u_int32_t fnv32_hash(const char *str, int len) {
    unsigned char *s = (unsigned char *)str;	/* unsigned string */
    // FNV-1a 
    /* See the FNV parameters at www.isthe.com/chongo/tech/comp/fnv/#FNV-param */
    const uint32_t FNV_32_PRIME = 0x01000193; /* 16777619 */

    uint32_t h = 0x811c9dc5; /* 2166136261 */
    while (len--) {
        /* xor the bottom with the current octet */
        h ^= *s++;
        /* multiply by the 32 bit FNV magic prime mod 2^32 */
        h *= FNV_32_PRIME;
    }

    return h;
}

static u_int32_t Crc32_ComputeBuf(u_int32_t crc32, const void *buf, int buflen)

{

    static const u_int32_t crcTable[256] = {

        0x00000000,0x77073096,0xEE0E612C,0x990951BA,0x076DC419,0x706AF48F,0xE963A535,

        0x9E6495A3,0x0EDB8832,0x79DCB8A4,0xE0D5E91E,0x97D2D988,0x09B64C2B,0x7EB17CBD,

        0xE7B82D07,0x90BF1D91,0x1DB71064,0x6AB020F2,0xF3B97148,0x84BE41DE,0x1ADAD47D,

        0x6DDDE4EB,0xF4D4B551,0x83D385C7,0x136C9856,0x646BA8C0,0xFD62F97A,0x8A65C9EC,

        0x14015C4F,0x63066CD9,0xFA0F3D63,0x8D080DF5,0x3B6E20C8,0x4C69105E,0xD56041E4,

        0xA2677172,0x3C03E4D1,0x4B04D447,0xD20D85FD,0xA50AB56B,0x35B5A8FA,0x42B2986C,

        0xDBBBC9D6,0xACBCF940,0x32D86CE3,0x45DF5C75,0xDCD60DCF,0xABD13D59,0x26D930AC,

        0x51DE003A,0xC8D75180,0xBFD06116,0x21B4F4B5,0x56B3C423,0xCFBA9599,0xB8BDA50F,

        0x2802B89E,0x5F058808,0xC60CD9B2,0xB10BE924,0x2F6F7C87,0x58684C11,0xC1611DAB,

        0xB6662D3D,0x76DC4190,0x01DB7106,0x98D220BC,0xEFD5102A,0x71B18589,0x06B6B51F,

        0x9FBFE4A5,0xE8B8D433,0x7807C9A2,0x0F00F934,0x9609A88E,0xE10E9818,0x7F6A0DBB,

        0x086D3D2D,0x91646C97,0xE6635C01,0x6B6B51F4,0x1C6C6162,0x856530D8,0xF262004E,

        0x6C0695ED,0x1B01A57B,0x8208F4C1,0xF50FC457,0x65B0D9C6,0x12B7E950,0x8BBEB8EA,

        0xFCB9887C,0x62DD1DDF,0x15DA2D49,0x8CD37CF3,0xFBD44C65,0x4DB26158,0x3AB551CE,

        0xA3BC0074,0xD4BB30E2,0x4ADFA541,0x3DD895D7,0xA4D1C46D,0xD3D6F4FB,0x4369E96A,

        0x346ED9FC,0xAD678846,0xDA60B8D0,0x44042D73,0x33031DE5,0xAA0A4C5F,0xDD0D7CC9,

        0x5005713C,0x270241AA,0xBE0B1010,0xC90C2086,0x5768B525,0x206F85B3,0xB966D409,

        0xCE61E49F,0x5EDEF90E,0x29D9C998,0xB0D09822,0xC7D7A8B4,0x59B33D17,0x2EB40D81,

        0xB7BD5C3B,0xC0BA6CAD,0xEDB88320,0x9ABFB3B6,0x03B6E20C,0x74B1D29A,0xEAD54739,

        0x9DD277AF,0x04DB2615,0x73DC1683,0xE3630B12,0x94643B84,0x0D6D6A3E,0x7A6A5AA8,

        0xE40ECF0B,0x9309FF9D,0x0A00AE27,0x7D079EB1,0xF00F9344,0x8708A3D2,0x1E01F268,

        0x6906C2FE,0xF762575D,0x806567CB,0x196C3671,0x6E6B06E7,0xFED41B76,0x89D32BE0,

        0x10DA7A5A,0x67DD4ACC,0xF9B9DF6F,0x8EBEEFF9,0x17B7BE43,0x60B08ED5,0xD6D6A3E8,

        0xA1D1937E,0x38D8C2C4,0x4FDFF252,0xD1BB67F1,0xA6BC5767,0x3FB506DD,0x48B2364B,

        0xD80D2BDA,0xAF0A1B4C,0x36034AF6,0x41047A60,0xDF60EFC3,0xA867DF55,0x316E8EEF,

        0x4669BE79,0xCB61B38C,0xBC66831A,0x256FD2A0,0x5268E236,0xCC0C7795,0xBB0B4703,

        0x220216B9,0x5505262F,0xC5BA3BBE,0xB2BD0B28,0x2BB45A92,0x5CB36A04,0xC2D7FFA7,

        0xB5D0CF31,0x2CD99E8B,0x5BDEAE1D,0x9B64C2B0,0xEC63F226,0x756AA39C,0x026D930A,

        0x9C0906A9,0xEB0E363F,0x72076785,0x05005713,0x95BF4A82,0xE2B87A14,0x7BB12BAE,

        0x0CB61B38,0x92D28E9B,0xE5D5BE0D,0x7CDCEFB7,0x0BDBDF21,0x86D3D2D4,0xF1D4E242,

        0x68DDB3F8,0x1FDA836E,0x81BE16CD,0xF6B9265B,0x6FB077E1,0x18B74777,0x88085AE6,

        0xFF0F6A70,0x66063BCA,0x11010B5C,0x8F659EFF,0xF862AE69,0x616BFFD3,0x166CCF45,

        0xA00AE278,0xD70DD2EE,0x4E048354,0x3903B3C2,0xA7672661,0xD06016F7,0x4969474D,

        0x3E6E77DB,0xAED16A4A,0xD9D65ADC,0x40DF0B66,0x37D83BF0,0xA9BCAE53,0xDEBB9EC5,

        0x47B2CF7F,0x30B5FFE9,0xBDBDF21C,0xCABAC28A,0x53B39330,0x24B4A3A6,0xBAD03605,

        0xCDD70693,0x54DE5729,0x23D967BF,0xB3667A2E,0xC4614AB8,0x5D681B02,0x2A6F2B94,

        0xB40BBE37,0xC30C8EA1,0x5A05DF1B,0x2D02EF8D };

    unsigned char *pbuf = (unsigned char*)buf;

    int i;

    int iLookup;

    for (i=0; i < buflen; i++) {

        iLookup = (crc32 & 0xFF) ^ (*pbuf++);

        crc32 = ((crc32 & 0xFFFFFF00) >> 8) & 0xFFFFFF;  // ' nasty shr 8 with vb :/

        crc32 = crc32 ^ crcTable[iLookup];

    }  
    return crc32;
}

u_int32_t crc32_of_buffer(const void *buf, int buflen)

{
    return Crc32_ComputeBuf(0xFFFFFFFF, buf, buflen) ^ 0xFFFFFFFF;
}

struct time {
    time_t tv_sec;
    suseconds_t tv_usec;
};

struct messege {
    int id; // 0,1,2,... id of packet
    char type; // 0 -> store data / 1 -> do somthing else with data
    int num_bytes; // how many bytes in buffer are valid -> other data are trash
    char buf[1000]; // data information
    u_int32_t CRC;
    u_int32_t HASH;
};

struct ACK_information {
    int id;
    char type;
};

//////////////////////////////////////////////////////////
void print_elements(struct messege *ptr){
    printf("structsize : %ld\n", sizeof(struct messege));
    printf("id: %d\n", ptr->id);
    printf("type: %d\n", ptr->type);
    printf("CRC: %" PRIu32 "\n", ptr->CRC);
    printf("numB: %d\n", ptr->num_bytes);
    printf("buf: %s\n", ptr->buf);
}

void generate_HASH_CRC(struct messege *to_be_send){
    to_be_send->CRC = crc32_of_buffer((void*)to_be_send, DATA_LENGTH_CRC);
    to_be_send->HASH = fnv32_hash((void*)to_be_send, DATA_LENGTH_HASH);
}

int check_CRC_HASH(struct messege *recieved){
    u_int32_t generated_CRC = crc32_of_buffer((void*)recieved, DATA_LENGTH_CRC);
    u_int32_t generated_HASH = fnv32_hash((void*)recieved, DATA_LENGTH_HASH);

    if (recieved->CRC != generated_CRC){
        printf("MESSEGE WITH WRONG CRC \n");
        return 1;
    } else if(recieved->HASH != generated_HASH){
        printf("MESSEGE WITH WRONG HASH \n");
        return 2;
    } else {
        printf("EVERYTHING OK \n");
        return 0;
    }
}

//////////////////////////////////////////////////////////

void check_input_window(int *window){
    if (*window < 0)
        *window = 1;
    else if (*window > 10)
        *window = 10;
}
void copy_to_send_array(struct messege *send_array, struct messege *to_be_send, int idx){
    
    send_array[idx].id = to_be_send->id;
    send_array[idx].type = to_be_send->type;
    send_array[idx].num_bytes = to_be_send->num_bytes;
    send_array[idx].CRC = to_be_send->CRC;
    send_array[idx].HASH = to_be_send->HASH;

    for(int j = 0; j < 1000; j++){
        send_array[idx].buf[j] = to_be_send->buf[j];
    }
}

void copy_to_to_be_send(struct messege *send_array, struct messege *to_be_send, int idx){
    
    to_be_send->id = send_array[idx].id;
    to_be_send->type = send_array[idx].type;
    to_be_send->num_bytes = send_array[idx].num_bytes;
    to_be_send->CRC = send_array[idx].CRC;
    to_be_send->HASH = send_array[idx].HASH;

    for(int j = 0; j < 1000; j++){
        to_be_send->buf[j] = send_array[idx].buf[j];
    }
}

void init_ACKs(struct ACK_information *ACKs, int window){
        for(int i = 0; i < window; i++){
            ACKs[i].type = ACK_FAIL;
            ACKs[i].id = i;
    }
}
//////////////////////////////////////////////////////////

int main(){

    struct time tv;
    struct messege *recieved;
    struct messege *to_be_send;  
    struct messege *send_array;
    struct ACK_information *ACKs;
    struct ACK_information *ACKs_fin;

    int window;
    printf("Type number of packets to be send in one window (max 10): ");
    scanf("%d", &window);
    check_input_window(&window);

    recieved = (struct messege*)calloc(1,sizeof(struct messege));    
    to_be_send = (struct messege*)calloc(1,sizeof(struct messege));    
    send_array = (struct messege*)calloc(window,sizeof(struct messege)); 
    ACKs = (struct ACK_information*)calloc(window,sizeof(struct ACK_information)); 
    ACKs_fin = (struct ACK_information*)calloc(window,sizeof(struct ACK_information));
    init_ACKs(ACKs, window);

    int sockfd, nBytes;

    
    struct sockaddr_in reciever_addr,sender_addr;
    sockfd=socket(AF_INET,SOCK_DGRAM,0);

    reciever_addr.sin_family = AF_INET;
    reciever_addr.sin_addr.s_addr=inet_addr(IP_ADDRESS);
    reciever_addr.sin_port=htons(PORT_NO_RECIEVER); //destination port for incoming packets


    sender_addr.sin_family = AF_INET;
    sender_addr.sin_addr.s_addr= htonl(INADDR_ANY);
    sender_addr.sin_port=htons(PORT_NO_SENDER); //source port for outgoing packets

    if (bind(sockfd,(struct sockaddr *)&sender_addr,sizeof(sender_addr)) == 0)

        printf("\nSuccessfully binded!\n");

    else

        printf("\nBinding Failed!\n");

    //int sockfd, nBytes;
    int addrlen_recv = sizeof(reciever_addr);
    int addrlen_send = sizeof(sender_addr);
    /*
    addr_con.sin_family = AF_INET;
    addr_con.sin_port = htons(PORT_NO);
    addr_con.sin_addr.s_addr = inet_addr(IP_ADDRESS);//inet_addr(IP_ADRESS);//INADDR_ANY;
    char net_buf[NET_BUF_SIZE];
    */
    FILE* fp;

    //sockfd = socket(AF_INET, SOCK_DGRAM, IP_PROTOCOL);
    tv.tv_sec = 2;
    tv.tv_usec = 0;
    setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(tv));

    /*
    if (connect(sockfd, (struct sockaddr*)&addr_con, sizeof(addr_con)) == 0){
        printf(" ----- CONECTED -----\n");
    } else{
        printf(" ----- NOT CONECTED -----\n");
    }*/

    // send file name + data //
    int load_id = 0;

    int condition1 = 1;
    int load_file_completed = 0;
    int receiver_has_all_data = 0;
    int first_messege = 1;

    while (condition1) {
        printf("Type file name to send: ");
        scanf("%s", to_be_send->buf);

        fp = fopen(to_be_send->buf, "rb");

        if (fp == NULL){
            printf("NO FILE WITH GIVEN NAME: %s\n", to_be_send->buf);
        } else {
            int condition2 = 1;
            while(condition2) {

                for (int i = 0; i < window; i++){
                    printf("id: %d \n", ACKs[i].id);
                    printf("type: %d \n", ACKs[i].type);
                }

                // LOADING PART
                // if not load_file_completed
                if(!load_file_completed){
                    for (int i = 0; i < window; i++){
                        if (first_messege){
                            // sending file name

                            if(load_id == 0){
                                to_be_send->type = SEND_FILE;
                            }else{
                                to_be_send->type = STORE;
                                to_be_send->num_bytes = fread(to_be_send->buf, 1, 1000, fp);

                                // NO MORE DATA TO BE SEND 
                                printf("načtené byty: %d \n",to_be_send->num_bytes);
                                if (to_be_send->num_bytes == 0){
                                    to_be_send->type = EMPTY;
                                    load_file_completed = 1;
                                }
                            }
                            to_be_send->id = load_id;
                            generate_HASH_CRC(to_be_send);
                            copy_to_send_array(send_array, to_be_send, i);
                            load_id++;
                        } else {
                            for(int j = 0; j < window; j++){
                                if( ACKs[j].id == send_array[i].id && ACKs[j].type == ACK_OK) {
                                    // send data (id > 0)
                                    to_be_send->type = STORE;
                                    to_be_send->id = load_id;
                                    to_be_send->num_bytes = fread(to_be_send->buf, 1, 1000, fp);

                                    // NO MORE DATA TO BE SEND 
                                    printf("načtené byty: %d \n",to_be_send->num_bytes);
                                    if (to_be_send->num_bytes == 0){
                                        to_be_send->type = EMPTY;
                                        load_file_completed = 1;
                                    }

                                    printf("packet n.%i\n", i);
                                    printf("id: %d \n", to_be_send->id);
                                    printf("type: %d \n", to_be_send->type);
                                    printf("num_bytes: %d \n", to_be_send->num_bytes);
                                    printf("\n");

                                    generate_HASH_CRC(to_be_send);
                                    copy_to_send_array(send_array, to_be_send, i);

                                    load_id++;
                                    break;
                                }
                            }
                        }
                    }

                    if (load_file_completed){
                        for(int i = 0; i < window; i++){
                            ACKs_fin[i].id = send_array[i].id;
                            ACKs_fin[i].type = ACK_FAIL;
                        }
                    }

                } else {
                    // load_file_completed

                    // check if reciever got all packets with data
                    for (int j = 0; j < window; j++){
                        for (int i = 0; i < window; i++){
                            if ((ACKs[j].type == ACK_OK) && (ACKs[j].id == ACKs_fin[i].id) && ACKs_fin[i].type != ACK_OK){
                                ACKs_fin[i].type = ACK_OK;
                            }
                        }
                    }

                    for (int i = 0; i < window; i++){
                        if(ACKs_fin[i].type != ACK_OK){
                            break;
                        }
                        if (i == window-1){
                            receiver_has_all_data = 1;
                        }
                    }

                    // reciever has all data -> send sending DONE
                    for(int i = 0; i < window; i++){
                        to_be_send->id = load_id+1;
                        to_be_send->type = SENDING_DONE;
                        generate_HASH_CRC(to_be_send);
                        copy_to_send_array(send_array, to_be_send, i);
                    }
                }


                // SEND
                first_messege = 0; 
                for (int ii = 0; ii < window; ii++){
                    copy_to_to_be_send(send_array, to_be_send, ii);
                    printf("id: %d\n", to_be_send->id);
                    printf("type: %d\n", to_be_send->type);
                    printf("num_bytes: %d\n", to_be_send->num_bytes);
                    printf("\n");

                    sendto(sockfd, (struct messege*)to_be_send, NET_BUF_SIZE, sendrecvflag, (struct sockaddr*)&reciever_addr, addrlen_recv);
                    //sleep(0.01); // sleep 10 ms
                }

                // HEAR for answer

                for (int iii = 0; iii < window; iii++){
                    nBytes = recvfrom(sockfd, (struct messege*)recieved, NET_BUF_SIZE, sendrecvflag, (struct sockaddr*)&sender_addr, &addrlen_send);

                    if (nBytes == -1){
                        printf("\r waiting for response...\n");
                        ACKs[iii].id = -1;
                        ACKs[iii].type = ACK_FAIL;
                    } else if(check_CRC_HASH(recieved)){
                        // wrong CRC or HASH
                        ACKs[iii].id = -1;
                        ACKs[iii].type = ACK_FAIL;
                    } else {
                        // CRC and HASK -> OK
                        ACKs[iii].id = recieved->id;
                        ACKs[iii].type = recieved->type;

                        printf("---- recvfrom ----\n");
                        printf("id: %d\n", recieved->id);
                        printf("type: %d\n", recieved->type);
                        printf("\n");     

                        if(recieved->type == SENDING_DONE){
                            free(recieved);
                            free(to_be_send);
                            free(send_array);
                            free(ACKs);
                            fclose(fp);
                            printf("AWESOME! SENDING DONE =)\n");
                            return 0;
                        }
                    }

                }
            }
        }
    }
    free(recieved);
    free(to_be_send);
    return 0;
}
