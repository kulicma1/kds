// client code for UDP socket programming
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>

#define SEND_FILE 0
#define STORE 1
#define ACK_OK 2
#define ACK_FAIL 3
#define SENDING_DONE 4  
#define WRONG_FILE 5 // error, no file with given name, stop sending
#define EXIT 6
#define IP_PROTOCOL 0
#define IP_ADDRESS "127.0.0.1" // localhost "10.0.0.143" // 
#define PORT_NO 15050
#define NET_BUF_SIZE 1048
#define cipherKey 'S'
#define sendrecvflag 0

struct messege {
    int id; // 0,1,2,... id of packet
    char type; // 0 -> store data / 1 -> do somthing else with data
    int num_bytes; // how many bytes in buffer are valid -> other data are trash
    char buf[1024]; // data information
    unsigned long CRC;
};
  
// function to clear buffer
void clearBuf(char* b)
{
    int i;
    for (i = 0; i < NET_BUF_SIZE; i++)
        b[i] = '\0';
}
  
// function for decryption
char Cipher(char ch)
{
    return ch ^ cipherKey;
}
  
// function to receive file
int recvFile(char* buf, int s)
{
    int i;
    char ch;
    for (i = 0; i < s; i++) {
        ch = buf[i];
        ch = Cipher(ch);
        if (ch == EOF)
            return 1;
        else
            printf("%c", ch);
    }
    return 0;
}
  
// driver code
int main()
{
    struct messege to_be_send;
    struct messege recieved; 
    int sockfd, nBytes;
    struct sockaddr_in addr_con;
    int addrlen = sizeof(addr_con);
    addr_con.sin_family = AF_INET;
    addr_con.sin_port = htons(PORT_NO);
    addr_con.sin_addr.s_addr = inet_addr(IP_ADDRESS);
    char net_buf[NET_BUF_SIZE];
    FILE* fp;
  
    // socket()
    sockfd = socket(AF_INET, SOCK_DGRAM, IP_PROTOCOL);
  
    if (sockfd < 0){
        printf("\nfile descriptor not received!!\n");
    } else {
        printf("\nfile descriptor %d received\n", sockfd);
    }
  
    while (1) {
        printf("\nPlease enter file name to receive:\n");
        to_be_send.type = SEND_FILE;
        scanf("%s", to_be_send.buf);

        sendto(sockfd, (struct messege*)&to_be_send, NET_BUF_SIZE,
               sendrecvflag, (struct sockaddr*)&addr_con,
               addrlen);
  
        printf("\n---------Data Received---------\n");

        fp = fopen("test_sended.jpg", "wb");
        printf("\nFile Name Received: %s\n", recieved.buf);

        if (fp == NULL){
            printf("\nFile open failed!\n");
        } else {
            while (1) {
                // receive
                nBytes = recvfrom(sockfd, (struct messege*)&recieved, NET_BUF_SIZE,
                                sendrecvflag, (struct sockaddr*)&addr_con,
                                &addrlen);

                // if ok -> store it
                // else -> send ACK_FAIL
                if (recieved.type == STORE){
                    fwrite(recieved.buf, 1, recieved.num_bytes, fp);

                    to_be_send.type = ACK_OK;
                    to_be_send.id = recieved.id;
                    printf("id: %d\n", recieved.id);
                    sendto(sockfd, (struct messege*)&to_be_send, NET_BUF_SIZE,
                            sendrecvflag, (struct sockaddr*)&addr_con,
                            addrlen);
                } else if (recieved.type == SENDING_DONE){
                    printf("SENDING DONE! AWESOME =)\n");
                    to_be_send.type = SENDING_DONE;
                    sendto(sockfd, (struct messege*)&to_be_send, NET_BUF_SIZE,
                            sendrecvflag, (struct sockaddr*)&addr_con,
                            addrlen);
                    return 0;                    
                } else if(recieved.type == WRONG_FILE){
                    printf("NO SUCH FILE\n");
                    break;     
                } else if (recieved.type == EXIT){
                    printf("EXIT \n");
                    return 0;    
                }else {
                    to_be_send.type = ACK_FAIL;
                    sendto(sockfd, (struct messege*)&to_be_send, NET_BUF_SIZE,
                            sendrecvflag, (struct sockaddr*)&addr_con,
                            addrlen);
                }
            }
            if (fp != NULL){
                fclose(fp);
            } 
        }
        printf("\n-------------------------------\n");
    }
    return 0;
}
/*
            while (1) {
                // receive
                clearBuf(net_buf);
                nBytes = recvfrom(sockfd, net_buf, NET_BUF_SIZE,
                                sendrecvflag, (struct sockaddr*)&addr_con,
                                &addrlen);

                // if ok -> store it
                // else -> send ACK_FAIL
    
                // process
                if (recvFile(net_buf, NET_BUF_SIZE)) {
                    break;
                }
            }
*/
