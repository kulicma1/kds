#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <inttypes.h>
#include <sys/time.h>

#define SEND_FILE 0
#define STORE 1
#define ACK_OK 2
#define ACK_FAIL 3
#define SENDING_DONE 4  
#define WRONG_FILE 5 // error, no file with given name, stop sending
#define EMPTY 6
#define DATA_LENGHT 1000
#define IP_PROTOCOL 0
#define IP_ADDRESS "127.0.0.1"//"192.168.1.162"//"127.0.0.1" // localhost "10.0.0.143" // 
#define IP_ADRESS_SENDER "127.0.0.1" //"192.168.12.251"
#define PORT_NO_RECIEVER 14000
#define PORT_NO_SENDER 15000
#define sendrecvflag 0
#define DATA_LENGTH_CRC 1012
#define DATA_LENGTH_HASH 1016
#define NET_BUF_SIZE 1020 // length of sended data

static u_int32_t fnv32_hash(const char *str, int len) {
    unsigned char *s = (unsigned char *)str;	/* unsigned string */
    // FNV-1a 
    /* See the FNV parameters at www.isthe.com/chongo/tech/comp/fnv/#FNV-param */
    const uint32_t FNV_32_PRIME = 0x01000193; /* 16777619 */

    uint32_t h = 0x811c9dc5; /* 2166136261 */
    while (len--) {
        /* xor the bottom with the current octet */
        h ^= *s++;
        /* multiply by the 32 bit FNV magic prime mod 2^32 */
        h *= FNV_32_PRIME;
    }

    return h;
}

static u_int32_t Crc32_ComputeBuf(u_int32_t crc32, const void *buf, int buflen)

{

    static const u_int32_t crcTable[256] = {

        0x00000000,0x77073096,0xEE0E612C,0x990951BA,0x076DC419,0x706AF48F,0xE963A535,

        0x9E6495A3,0x0EDB8832,0x79DCB8A4,0xE0D5E91E,0x97D2D988,0x09B64C2B,0x7EB17CBD,

        0xE7B82D07,0x90BF1D91,0x1DB71064,0x6AB020F2,0xF3B97148,0x84BE41DE,0x1ADAD47D,

        0x6DDDE4EB,0xF4D4B551,0x83D385C7,0x136C9856,0x646BA8C0,0xFD62F97A,0x8A65C9EC,

        0x14015C4F,0x63066CD9,0xFA0F3D63,0x8D080DF5,0x3B6E20C8,0x4C69105E,0xD56041E4,

        0xA2677172,0x3C03E4D1,0x4B04D447,0xD20D85FD,0xA50AB56B,0x35B5A8FA,0x42B2986C,

        0xDBBBC9D6,0xACBCF940,0x32D86CE3,0x45DF5C75,0xDCD60DCF,0xABD13D59,0x26D930AC,

        0x51DE003A,0xC8D75180,0xBFD06116,0x21B4F4B5,0x56B3C423,0xCFBA9599,0xB8BDA50F,

        0x2802B89E,0x5F058808,0xC60CD9B2,0xB10BE924,0x2F6F7C87,0x58684C11,0xC1611DAB,

        0xB6662D3D,0x76DC4190,0x01DB7106,0x98D220BC,0xEFD5102A,0x71B18589,0x06B6B51F,

        0x9FBFE4A5,0xE8B8D433,0x7807C9A2,0x0F00F934,0x9609A88E,0xE10E9818,0x7F6A0DBB,

        0x086D3D2D,0x91646C97,0xE6635C01,0x6B6B51F4,0x1C6C6162,0x856530D8,0xF262004E,

        0x6C0695ED,0x1B01A57B,0x8208F4C1,0xF50FC457,0x65B0D9C6,0x12B7E950,0x8BBEB8EA,

        0xFCB9887C,0x62DD1DDF,0x15DA2D49,0x8CD37CF3,0xFBD44C65,0x4DB26158,0x3AB551CE,

        0xA3BC0074,0xD4BB30E2,0x4ADFA541,0x3DD895D7,0xA4D1C46D,0xD3D6F4FB,0x4369E96A,

        0x346ED9FC,0xAD678846,0xDA60B8D0,0x44042D73,0x33031DE5,0xAA0A4C5F,0xDD0D7CC9,

        0x5005713C,0x270241AA,0xBE0B1010,0xC90C2086,0x5768B525,0x206F85B3,0xB966D409,

        0xCE61E49F,0x5EDEF90E,0x29D9C998,0xB0D09822,0xC7D7A8B4,0x59B33D17,0x2EB40D81,

        0xB7BD5C3B,0xC0BA6CAD,0xEDB88320,0x9ABFB3B6,0x03B6E20C,0x74B1D29A,0xEAD54739,

        0x9DD277AF,0x04DB2615,0x73DC1683,0xE3630B12,0x94643B84,0x0D6D6A3E,0x7A6A5AA8,

        0xE40ECF0B,0x9309FF9D,0x0A00AE27,0x7D079EB1,0xF00F9344,0x8708A3D2,0x1E01F268,

        0x6906C2FE,0xF762575D,0x806567CB,0x196C3671,0x6E6B06E7,0xFED41B76,0x89D32BE0,

        0x10DA7A5A,0x67DD4ACC,0xF9B9DF6F,0x8EBEEFF9,0x17B7BE43,0x60B08ED5,0xD6D6A3E8,

        0xA1D1937E,0x38D8C2C4,0x4FDFF252,0xD1BB67F1,0xA6BC5767,0x3FB506DD,0x48B2364B,

        0xD80D2BDA,0xAF0A1B4C,0x36034AF6,0x41047A60,0xDF60EFC3,0xA867DF55,0x316E8EEF,

        0x4669BE79,0xCB61B38C,0xBC66831A,0x256FD2A0,0x5268E236,0xCC0C7795,0xBB0B4703,

        0x220216B9,0x5505262F,0xC5BA3BBE,0xB2BD0B28,0x2BB45A92,0x5CB36A04,0xC2D7FFA7,

        0xB5D0CF31,0x2CD99E8B,0x5BDEAE1D,0x9B64C2B0,0xEC63F226,0x756AA39C,0x026D930A,

        0x9C0906A9,0xEB0E363F,0x72076785,0x05005713,0x95BF4A82,0xE2B87A14,0x7BB12BAE,

        0x0CB61B38,0x92D28E9B,0xE5D5BE0D,0x7CDCEFB7,0x0BDBDF21,0x86D3D2D4,0xF1D4E242,

        0x68DDB3F8,0x1FDA836E,0x81BE16CD,0xF6B9265B,0x6FB077E1,0x18B74777,0x88085AE6,

        0xFF0F6A70,0x66063BCA,0x11010B5C,0x8F659EFF,0xF862AE69,0x616BFFD3,0x166CCF45,

        0xA00AE278,0xD70DD2EE,0x4E048354,0x3903B3C2,0xA7672661,0xD06016F7,0x4969474D,

        0x3E6E77DB,0xAED16A4A,0xD9D65ADC,0x40DF0B66,0x37D83BF0,0xA9BCAE53,0xDEBB9EC5,

        0x47B2CF7F,0x30B5FFE9,0xBDBDF21C,0xCABAC28A,0x53B39330,0x24B4A3A6,0xBAD03605,

        0xCDD70693,0x54DE5729,0x23D967BF,0xB3667A2E,0xC4614AB8,0x5D681B02,0x2A6F2B94,

        0xB40BBE37,0xC30C8EA1,0x5A05DF1B,0x2D02EF8D };

    unsigned char *pbuf = (unsigned char*)buf;

    int i;

    int iLookup;

    for (i=0; i < buflen; i++) {

        iLookup = (crc32 & 0xFF) ^ (*pbuf++);

        crc32 = ((crc32 & 0xFFFFFF00) >> 8) & 0xFFFFFF;  // ' nasty shr 8 with vb :/

        crc32 = crc32 ^ crcTable[iLookup];

    }  
    return crc32;
}

u_int32_t crc32_of_buffer(const void *buf, int buflen)

{
    return Crc32_ComputeBuf(0xFFFFFFFF, buf, buflen) ^ 0xFFFFFFFF;
}

struct time {
    time_t tv_sec;
    suseconds_t tv_usec;
};

struct messege {
    int id; // 0,1,2,... id of packet
    char type; // 0 -> store data / 1 -> do somthing else with data
    int num_bytes; // how many bytes in buffer are valid -> other data are trash
    char buf[1000]; // data information
    u_int32_t CRC;
    u_int32_t HASH;
};

//////////////////////////////////////////////////////////
void print_elements(struct messege *ptr){
    printf("structsize : %ld\n", sizeof(struct messege));
    printf("id: %d\n", ptr->id);
    printf("type: %d\n", ptr->type);
    printf("CRC: %" PRIu32 "\n", ptr->CRC);
    printf("HASH: %" PRIu32 "\n", ptr->HASH);
    printf("numB: %d\n", ptr->num_bytes);
    printf("buf: %s\n", ptr->buf);
}

void generate_HASH_CRC(struct messege *to_be_send){
    to_be_send->CRC = crc32_of_buffer((void*)to_be_send, DATA_LENGTH_CRC);
    to_be_send->HASH = fnv32_hash((void*)to_be_send, DATA_LENGTH_HASH);
}

int check_CRC_HASH(struct messege *recieved){
    u_int32_t generated_CRC = crc32_of_buffer((void*)recieved, DATA_LENGTH_CRC);
    u_int32_t generated_HASH = fnv32_hash((void*)recieved, DATA_LENGTH_HASH);

    if (recieved->CRC != generated_CRC){
        printf("MESSEGE WITH WRONG CRC \n");
        return 1;
    } else if(recieved->HASH != generated_HASH){
        printf("MESSEGE WITH WRONG HASH \n");
        return 2;
    } else {
        printf("EVERYTHING OK \n");
        return 0;
    }
}

///////////////////////////////////////////////////////////

int main(){
    
    struct time tv;
    struct messege *recieved;
    struct messege *to_be_send;    
    struct sockaddr_in addr_reciever, addr_sender;

    recieved = (struct messege*)calloc(1,sizeof(struct messege));    
    to_be_send = (struct messege*)calloc(1,sizeof(struct messege));    

    int sockfd, nBytes;

    int addrlen_reciever = sizeof(addr_reciever);
    addr_reciever.sin_family = AF_INET;
    addr_reciever.sin_port = htons(PORT_NO_RECIEVER);
    addr_reciever.sin_addr.s_addr = htonl(INADDR_ANY); //inet_addr(IP_ADDRESS);

    int addrlen_sender = sizeof(addr_sender);
    addr_sender.sin_family = AF_INET;
    addr_sender.sin_port = htons(PORT_NO_SENDER);
    addr_sender.sin_addr.s_addr = inet_addr(IP_ADRESS_SENDER); //inet_addr(IP_ADDRESS);   


    FILE* fp;

    // socket()
    sockfd = socket(AF_INET, SOCK_DGRAM, IP_PROTOCOL);
    tv.tv_sec = 2;
    tv.tv_usec = 0;
    setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(tv));

        // bind()
    if (bind(sockfd, (struct sockaddr*)&addr_reciever, sizeof(addr_reciever)) == 0)
        printf("\nSuccessfully binded!\n");
    else
        printf("\nBinding Failed!\n");

    printf(" --- Waiting for file name --- \n");

    int condition = 1;

    int cursor_on_id = 1;
    int max_cursor_id = 1;

    int file_opened = 0;

    while(condition){
            nBytes = recvfrom(sockfd, (struct messege*)recieved, NET_BUF_SIZE, sendrecvflag, (struct sockaddr*)&addr_reciever, &addrlen_reciever);
            printf("runs...\n");
            if (nBytes == -1) {
                // waiting ..
            } else if (check_CRC_HASH(recieved)){
                //WRONG CRC / HASH
                to_be_send->type = ACK_FAIL;
                generate_HASH_CRC(to_be_send);
                sendto(sockfd, (struct messege*)to_be_send, NET_BUF_SIZE, sendrecvflag, (struct sockaddr*)&addr_sender, addrlen_sender);
                
            } else if (!file_opened && recieved->type == SEND_FILE && recieved->id == 0){
                    // 1. messege with filename
                    printf("filename: %s\n", recieved->buf);
                    //fp = fopen(recieved->buf, "wb");
                    fp = fopen("fin.jpg", "wb");
                    if (fp == NULL){
                        printf("Can not open file\n");
                        exit(1);
                    }
                    to_be_send->type = ACK_OK;
                    generate_HASH_CRC(to_be_send);
                    sendto(sockfd, (struct messege*)to_be_send, NET_BUF_SIZE, sendrecvflag, (struct sockaddr*)&addr_sender, addrlen_sender); 
                    file_opened = 1;                 
            }   else if (file_opened && recieved->type == STORE){
                    // SAVE DATA

                     // write to data to file
                        if (recieved->id > max_cursor_id){
                            // write far away from already written data
                            int move_cursor = max_cursor_id - cursor_on_id; 
                            fseek(fp, move_cursor*DATA_LENGHT, SEEK_CUR);

                            int write_temp_data = recieved->id - max_cursor_id;

                            // fill empty space with zeros
                            char write = 0x00;
                            for (int iter = 0; iter < write_temp_data; iter++){
                                for (int ii = 0; ii < DATA_LENGHT; ii++){
                                    fwrite(&write, 1, 1, fp);
                                }
                            }

                            // write data 
                            fwrite(recieved->buf, 1, recieved->num_bytes, fp);

                            max_cursor_id = recieved->id+1;
                            cursor_on_id = max_cursor_id;

                        } else {
                            // write it on corect place
                            int move_cursor = recieved->id - cursor_on_id;
                            if(move_cursor != 0){
                                fseek(fp, move_cursor*DATA_LENGHT, SEEK_CUR);
                            }
                            fwrite(recieved->buf, 1, recieved->num_bytes, fp);
                            cursor_on_id = recieved->id + 1;

                            if (cursor_on_id > max_cursor_id){
                                max_cursor_id = cursor_on_id;
                            }
                        }

                        to_be_send->type = ACK_OK;
                        to_be_send->id = recieved->id;
                        generate_HASH_CRC(to_be_send);
                        sendto(sockfd, (struct messege*)to_be_send, NET_BUF_SIZE, sendrecvflag, (struct sockaddr*)&addr_sender, addrlen_sender); 
                        printf("poslal jsem id: %d\n", to_be_send->id); 
                        printf("type: %d\n", to_be_send->type);

            } else if (recieved->type == SENDING_DONE){
                    to_be_send->type = SENDING_DONE;
                    generate_HASH_CRC(to_be_send);
                    sendto(sockfd, (struct messege*)to_be_send, NET_BUF_SIZE, sendrecvflag, (struct sockaddr*)&addr_sender, addrlen_sender);
                    free(recieved);
                    free(to_be_send);
                    fclose(fp);
                    printf("SENDING DONE =)\n");
                    return 0;
            } else if (recieved->type == EMPTY) {
                    printf("EMPTY\n");
                    to_be_send->type = ACK_OK;
                    generate_HASH_CRC(to_be_send);
                    sendto(sockfd, (struct messege*)to_be_send, NET_BUF_SIZE, sendrecvflag, (struct sockaddr*)&addr_sender, addrlen_sender);                    
            } else if (file_opened && recieved->type == SEND_FILE) {
                    printf("Again name of file\n");
                    to_be_send->type = ACK_OK;
                    generate_HASH_CRC(to_be_send);
                    sendto(sockfd, (struct messege*)to_be_send, NET_BUF_SIZE, sendrecvflag, (struct sockaddr*)&addr_sender, addrlen_sender);                    
            }else {
                    // OTHERWISE
                    printf("else\n");
                    to_be_send->type = ACK_FAIL;
                    generate_HASH_CRC(to_be_send);
                    sendto(sockfd, (struct messege*)to_be_send, NET_BUF_SIZE, sendrecvflag, (struct sockaddr*)&addr_sender, addrlen_sender);
            }
    }
    free(recieved);
    free(to_be_send);
    return 0;
}
