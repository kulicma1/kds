// server code for UDP socket programming
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>
  
#define SEND_FILE 0
#define STORE 1
#define ACK_OK 2
#define ACK_FAIL 3
#define SENDING_DONE 4
#define WRONG_FILE 5 // error, no file with given name, stop sending
#define EXIT 6
#define IP_PROTOCOL 0
#define PORT_NO 15050
#define NET_BUF_SIZE 1048
#define cipherKey 'S'
#define sendrecvflag 0
#define nofile "File Not Found!"
#define IP_ADRESS "10.34.3.33"

struct messege {
    int id; // 0,1,2,... id of packet
    char type; // SEND_FILE/STORE/ACK_OK/ACK_FAIL
    int num_bytes; // how many bytes in buffer are valid -> other data are trash
    char buf[1024]; // data information
    unsigned long CRC;
};

  
// function to clear buffer
void clearBuf(char* b)
{
    int i;
    for (i = 0; i < NET_BUF_SIZE; i++)
        b[i] = '\0';
}
  
// function to encrypt
char Cipher(char ch)
{
    return ch ^ cipherKey;
}
  
// function sending file
int sendFile(FILE* fp, char* buf, int s)
{
    int i, len;
    if (fp == NULL) {
        strcpy(buf, nofile);
        len = strlen(nofile);
        buf[len] = EOF;
        for (i = 0; i <= len; i++)
            buf[i] = Cipher(buf[i]);
        return 1;
    }
  
    char ch, ch2;
    for (i = 0; i < s; i++) {
        ch = fgetc(fp);
        ch2 = Cipher(ch);
        buf[i] = ch2;
        if (ch == EOF)
            return 1;
    }
    return 0;
}
  
// driver code
int main() {
    struct messege to_be_send;
    struct messege recieved; 
    int sockfd, nBytes;
    struct sockaddr_in addr_con;
    int addrlen = sizeof(addr_con);
    addr_con.sin_family = AF_INET;
    addr_con.sin_port = htons(PORT_NO);
    addr_con.sin_addr.s_addr = INADDR_ANY;//inet_addr(IP_ADRESS);//INADDR_ANY;
    char net_buf[NET_BUF_SIZE];
    FILE* fp;
  
    // socket()
    sockfd = socket(AF_INET, SOCK_DGRAM, IP_PROTOCOL);
  
    if (sockfd < 0)
        printf("\nfile descriptor not received!!\n");
    else
        printf("\nfile descriptor %d received\n", sockfd);
  
    // bind()
    if (bind(sockfd, (struct sockaddr*)&addr_con, sizeof(addr_con)) == 0)
        printf("\nSuccessfully binded!\n");
    else
        printf("\nBinding Failed!\n");
  
    while (1) {
        printf("\nWaiting for file name...\n");
  
        // receive file name
        nBytes = recvfrom(sockfd, (struct messege*)&recieved,
                          NET_BUF_SIZE, sendrecvflag,
                          (struct sockaddr*)&addr_con, &addrlen);

        if (nBytes != NET_BUF_SIZE){
            printf("ERROR: přišlo %d bytů místo %d", nBytes, NET_BUF_SIZE);
            return 1;
        }

        if (recieved.type){
            printf("Invalid type of packet for sending name of file to be send\n");

        } else {
            fp = fopen(recieved.buf, "rb");
            printf("\nFile Name Received: %s\n", recieved.buf);

            if (fp == NULL){
                printf("\nFile open failed!\n");
                to_be_send.type = WRONG_FILE;
                sendto(sockfd, (struct messege*)&to_be_send, NET_BUF_SIZE,
                        sendrecvflag, 
                        (struct sockaddr*)&addr_con, addrlen);
            } else {
                printf("\nFile Successfully opened!\n");

                // prepare to_be_send for begining of sending data
                to_be_send.id = 0;
                to_be_send.type = STORE;
                // begin sending data of file
                bool DONE = 0;
                
                while (!DONE) {
                    to_be_send.num_bytes = fread(&to_be_send.buf, 1, 124, fp);

                    if (to_be_send.num_bytes == 0){
                        DONE = 1;
                        to_be_send.type = SENDING_DONE;
                    }

                    // process
                    sendto(sockfd, (struct messege*)&to_be_send, NET_BUF_SIZE,
                            sendrecvflag, 
                            (struct sockaddr*)&addr_con, addrlen);

                    recvfrom(sockfd, (struct messege*)&recieved,
                          NET_BUF_SIZE, sendrecvflag,
                          (struct sockaddr*)&addr_con, &addrlen);

                    if (recieved.type == ACK_OK){
                        to_be_send.id++;
                    } else if(recieved.type == SENDING_DONE){
                        printf("SENDING DONE! AWESOME =)\n");
                        return 0;
                    } else {
                        printf("Recieved type: %d\n", recieved.type);
                        fseek(fp, -to_be_send.num_bytes, SEEK_CUR);
                        DONE = 0;
                        to_be_send.type = STORE;
                    }
                }
            }
        }

        if (fp != NULL){
            fclose(fp);
        }        
    }
    printf("Aplication closed");
    return 0;
}


        /* fp = fopen(net_buf, "r");
        printf("\nFile Name Received: %s\n", net_buf);
        if (fp == NULL)
            printf("\nFile open failed!\n");
        else
            printf("\nFile Successfully opened!\n");
  
        while (1) {
  
            // process
            if (sendFile(fp, net_buf, NET_BUF_SIZE)) {
                sendto(sockfd, net_buf, NET_BUF_SIZE,
                       sendrecvflag, 
                    (struct sockaddr*)&addr_con, addrlen);
                break;
            }
  
            // send
            sendto(sockfd, net_buf, NET_BUF_SIZE,
                   sendrecvflag,
                (struct sockaddr*)&addr_con, addrlen);
            clearBuf(net_buf);
        }
        if (fp != NULL)
            fclose(fp);
    }
    return 0;
}*/
